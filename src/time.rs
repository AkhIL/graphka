use chrono::{NaiveDateTime, Utc};

/// Unix timestamp as microseconds.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct DateTime(i64);

impl DateTime {
    pub const MIN: Self = Self(i64::MIN);
    pub const MAX: Self = Self(i64::MAX);

    pub fn now() -> Self {
        chrono::offset::Utc::now().into()
    }

    pub fn from_timestamp_micros(micros: i64) -> Self {
        Self(micros)
    }

    pub fn from_timestamp_millis(millis: i64) -> Self {
        Self(millis * 1_000)
    }

    pub fn from_timestamp_secs(secs: i64) -> Self {
        Self(secs * 1_000_000)
    }

    pub fn timestamp_micros(&self) -> i64 {
        self.0
    }

    pub fn timestamp_millis(&self) -> i64 {
        self.0 / 1_000
    }

    pub fn timestamp_secs(&self) -> i64 {
        self.0 / 1_000_000
    }
}

impl Default for DateTime {
    fn default() -> Self {
        0.into()
    }
}

impl From<chrono::DateTime<Utc>> for DateTime {
    fn from(value: chrono::DateTime<Utc>) -> Self {
        DateTime(value.timestamp_micros())
    }
}

impl From<DateTime> for chrono::DateTime<Utc> {
    fn from(value: DateTime) -> Self {
        chrono::DateTime::<Utc>::from_naive_utc_and_offset(
            NaiveDateTime::default() + chrono::Duration::microseconds(value.0),
            Utc,
        )
    }
}

impl From<i64> for DateTime {
    fn from(value: i64) -> Self {
        DateTime(value)
    }
}

impl From<DateTime> for i64 {
    fn from(value: DateTime) -> Self {
        value.0
    }
}

impl std::fmt::Display for DateTime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            chrono::DateTime::<Utc>::from(*self)
                .to_rfc3339_opts(chrono::SecondsFormat::Micros, false)
        )
    }
}

/// Duration as microseconds
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Duration(i64);

impl Duration {
    const MIN: Self = Self(i64::MIN);
    const MAX: Self = Self(i64::MAX);

    pub fn as_micros(&self) -> i64 {
        self.0
    }

    pub fn as_millis(&self) -> i64 {
        self.0 / 1_000
    }

    pub fn as_secs(&self) -> i64 {
        self.0 / 1_000_000
    }

    pub fn from_micros(micros: i64) -> Self {
        Self(micros)
    }

    pub fn from_millis(millis: i64) -> Self {
        Self::from_micros(millis * 1_000)
    }

    pub fn from_secs(secs: i64) -> Self {
        Self::from_micros(secs * 1_000_000)
    }
}

impl From<chrono::Duration> for Duration {
    fn from(value: chrono::Duration) -> Self {
        Duration(value.num_microseconds().expect("Value overflow"))
    }
}

impl From<Duration> for chrono::Duration {
    fn from(value: Duration) -> Self {
        chrono::Duration::microseconds(value.0)
    }
}

impl From<i64> for Duration {
    fn from(value: i64) -> Self {
        Duration(value)
    }
}

impl From<Duration> for i64 {
    fn from(value: Duration) -> Self {
        value.0
    }
}

impl std::fmt::Display for Duration {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", chrono::Duration::from(*self))
    }
}

impl std::ops::Add<Duration> for Duration {
    type Output = Self;
    fn add(self, rhs: Duration) -> Self::Output {
        Self(self.0 + rhs.0)
    }
}

impl std::ops::AddAssign<Duration> for Duration {
    fn add_assign(&mut self, rhs: Duration) {
        *self = *self + rhs
    }
}

impl std::ops::Sub<Duration> for Duration {
    type Output = Duration;
    fn sub(self, rhs: Duration) -> Self::Output {
        Self(self.0 - rhs.0)
    }
}

impl std::ops::SubAssign<Duration> for Duration {
    fn sub_assign(&mut self, rhs: Duration) {
        *self = *self - rhs
    }
}

impl std::ops::Neg for Duration {
    type Output = Self;
    fn neg(self) -> Self::Output {
        Self(-self.0)
    }
}

impl std::ops::Mul<i64> for Duration {
    type Output = Duration;
    fn mul(self, rhs: i64) -> Self::Output {
        Self(self.0 * rhs)
    }
}

impl std::ops::MulAssign<i64> for Duration {
    fn mul_assign(&mut self, rhs: i64) {
        *self = *self * rhs
    }
}

impl std::ops::Div<i64> for Duration {
    type Output = Duration;
    fn div(self, rhs: i64) -> Self::Output {
        Self(self.0 / rhs)
    }
}

impl std::ops::DivAssign<i64> for Duration {
    fn div_assign(&mut self, rhs: i64) {
        *self = *self / rhs
    }
}

impl std::ops::Add<Duration> for DateTime {
    type Output = Self;
    fn add(self, rhs: Duration) -> Self::Output {
        Self(self.0 + rhs.0)
    }
}

impl std::ops::AddAssign<Duration> for DateTime {
    fn add_assign(&mut self, rhs: Duration) {
        *self = *self + rhs
    }
}

impl std::ops::Sub<Duration> for DateTime {
    type Output = Self;
    fn sub(self, rhs: Duration) -> Self::Output {
        Self(self.0 - rhs.0)
    }
}

impl std::ops::SubAssign<Duration> for DateTime {
    fn sub_assign(&mut self, rhs: Duration) {
        *self = *self - rhs
    }
}

impl std::ops::Sub<DateTime> for DateTime {
    type Output = Duration;
    fn sub(self, rhs: DateTime) -> Self::Output {
        Duration(self.0 - rhs.0)
    }
}

#[cfg(test)]
mod tests {
    use chrono::Utc;

    use super::DateTime;

    #[test]
    fn datetime_chrono_conversion() {
        let datetime = DateTime::now();
        let datetime_chrono: chrono::DateTime<Utc> = datetime.into();
        let datetime_new: DateTime = datetime_chrono.into();
        assert_eq!(datetime, datetime_new);
    }

    #[test]
    fn negative_datetime_chrono_conversion() {
        let datetime = DateTime::from(-123456789);
        let datetime_chrono: chrono::DateTime<Utc> = datetime.into();
        let datetime_new: DateTime = datetime_chrono.into();
        dbg!(datetime, datetime_chrono, datetime_new);
        assert_eq!(datetime, datetime_new);
    }
}
