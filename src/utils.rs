use uuid::Uuid;

pub fn new_uuid() -> Uuid {
    let mac = mac_address::get_mac_address()
        .unwrap_or_default()
        .unwrap_or_default();
    Uuid::now_v1(&mac.bytes())
}
