mod channel;
mod event;
mod store;

const STORE_SERVICE_IS_DEAD: &str = "Event store service is dead";

pub use {
    channel::{
        ChannelError, ChannelHandle, ChannelInfo, ChannelMessage, ChannelUpdate, Direction, Query,
        WriterError, WriterHandle, WriterMessage,
    },
    event::{Event, EventHeader},
    store::{CreateError, StoreError, StoreHandle, StoreMessage},
};

pub mod rocksdb;
