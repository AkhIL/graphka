use bytes::Bytes;
use futures::{Future, FutureExt, Stream, TryFutureExt, TryStreamExt};
use std::sync::Arc;
use tokio::sync::{mpsc, oneshot};
use tokio_stream::wrappers::ReceiverStream;
use uuid::Uuid;

use crate::{time::DateTime, QUEUE_SIZE};

use super::{Event, EventHeader, StoreError, STORE_SERVICE_IS_DEAD};

#[derive(Debug, Clone, PartialEq)]
pub struct ChannelInfo {
    pub uuid: Uuid,
    pub name: Arc<str>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ChannelUpdate {
    First(EventHeader),
    Last(EventHeader),
}

#[derive(Clone)]
pub struct ChannelHandle {
    inbox: mpsc::Sender<ChannelMessage>,
}

impl ChannelHandle {
    pub fn get_first_header(
        &self,
    ) -> impl 'static + Future<Output = Result<Option<EventHeader>, ChannelError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.inbox.clone();
        async move {
            request_tx
                .send(ChannelMessage::GetFirstHader { reply_to: reply_tx })
                .map_err(|_| ChannelError::from(StoreError::ConnectionError(STORE_SERVICE_IS_DEAD)))
                .and_then(|_| {
                    reply_rx.unwrap_or_else(|_| {
                        Err(ChannelError::from(StoreError::ConnectionError(
                            STORE_SERVICE_IS_DEAD,
                        )))
                    })
                })
                .await
        }
    }
    pub fn get_last_header(
        &self,
    ) -> impl 'static + Future<Output = Result<Option<EventHeader>, ChannelError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.inbox.clone();
        async move {
            request_tx
                .send(ChannelMessage::GetLastHader { reply_to: reply_tx })
                .map_err(|_| ChannelError::from(StoreError::ConnectionError(STORE_SERVICE_IS_DEAD)))
                .and_then(|_| {
                    reply_rx.unwrap_or_else(|_| {
                        Err(ChannelError::from(StoreError::ConnectionError(
                            STORE_SERVICE_IS_DEAD,
                        )))
                    })
                })
                .await
        }
    }

    pub fn get_header_at(
        &self,
        datetime: DateTime,
    ) -> impl 'static + Future<Output = Result<Option<EventHeader>, ChannelError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.inbox.clone();
        async move {
            request_tx
                .send(ChannelMessage::GetHeaderAt {
                    datetime,
                    reply_to: reply_tx,
                })
                .map_err(|_| ChannelError::from(StoreError::ConnectionError(STORE_SERVICE_IS_DEAD)))
                .and_then(|_| {
                    reply_rx.unwrap_or_else(|_| {
                        Err(ChannelError::from(StoreError::ConnectionError(
                            STORE_SERVICE_IS_DEAD,
                        )))
                    })
                })
                .await
        }
    }

    pub fn watch_updates(
        &self,
    ) -> impl 'static + Stream<Item = Result<ChannelUpdate, ChannelError>> {
        let (reply_tx, reply_rx) = mpsc::channel(QUEUE_SIZE);
        let request_tx = self.inbox.clone();
        async move {
            request_tx
                .send(ChannelMessage::WatchUpdates { reply_to: reply_tx })
                .map_err(|_| ChannelError::from(StoreError::ConnectionError(STORE_SERVICE_IS_DEAD)))
                .map_ok(|_| ReceiverStream::from(reply_rx))
                .await
        }
        .into_stream()
        .try_flatten()
    }

    pub fn query(&self) -> QueryBuilder {
        QueryBuilder {
            query: Query::default(),
            inbox: self.inbox.clone(),
        }
    }

    pub fn get_writer(&self) -> impl 'static + Future<Output = Result<WriterHandle, WriterError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.inbox.clone();
        async move {
            request_tx
                .send(ChannelMessage::GetWriter { reply_to: reply_tx })
                .map_err(|_| WriterError::from(StoreError::ConnectionError(STORE_SERVICE_IS_DEAD)))
                .and_then(|_| {
                    reply_rx.unwrap_or_else(|_| {
                        Err(WriterError::from(StoreError::ConnectionError(
                            STORE_SERVICE_IS_DEAD,
                        )))
                    })
                })
                .await
        }
    }
}

pub enum ChannelMessage {
    Fetch {
        query: Query,
        reply_to: mpsc::Sender<Result<Event, ChannelError>>,
    },
    GetFirstHader {
        reply_to: oneshot::Sender<Result<Option<EventHeader>, ChannelError>>,
    },
    GetLastHader {
        reply_to: oneshot::Sender<Result<Option<EventHeader>, ChannelError>>,
    },
    GetHeaderAt {
        datetime: DateTime,
        reply_to: oneshot::Sender<Result<Option<EventHeader>, ChannelError>>,
    },
    WatchUpdates {
        reply_to: mpsc::Sender<Result<ChannelUpdate, ChannelError>>,
    },
    GetWriter {
        reply_to: oneshot::Sender<Result<WriterHandle, WriterError>>,
    },
}

#[derive(Debug, Default)]
pub struct Query {
    pub from_serial: Option<u64>,
    pub to_serial: Option<u64>,
    pub limit: Option<u64>,
    pub direction: Direction,
}

#[derive(Debug)]
pub enum Direction {
    Forward,
    Reversed,
}

impl Default for Direction {
    fn default() -> Self {
        Direction::Forward
    }
}

pub struct QueryBuilder {
    query: Query,
    inbox: mpsc::Sender<ChannelMessage>,
}

impl QueryBuilder {
    pub fn from(self, from_serial: u64) -> Self {
        let query = Query {
            from_serial: Some(from_serial),
            ..self.query
        };
        Self { query, ..self }
    }

    pub fn to(self, to_serial: u64) -> Self {
        let query = Query {
            to_serial: Some(to_serial),
            ..self.query
        };
        Self { query, ..self }
    }

    pub fn limit(self, limit: u64) -> Self {
        let query = Query {
            limit: Some(limit),
            ..self.query
        };
        Self { query, ..self }
    }

    pub fn reversed(self) -> Self {
        let query = Query {
            direction: Direction::Reversed,
            ..self.query
        };
        Self { query, ..self }
    }

    pub fn fetch(self) -> impl 'static + Stream<Item = Result<Event, ChannelError>> {
        let (reply_tx, reply_rx) = mpsc::channel(QUEUE_SIZE);
        async move {
            self.inbox
                .send(ChannelMessage::Fetch {
                    query: self.query,
                    reply_to: reply_tx,
                })
                .map_err(|_| ChannelError::from(StoreError::ConnectionError(STORE_SERVICE_IS_DEAD)))
                .map_ok(|_| ReceiverStream::from(reply_rx))
                .await
        }
        .into_stream()
        .try_flatten()
    }
}

#[derive(Debug, thiserror::Error)]
pub enum ChannelError {
    #[error("The channel does not exist")]
    ChannelNotExist,

    #[error(transparent)]
    StoreError(#[from] StoreError),
}

// Writer

pub struct WriterHandle {
    inbox: mpsc::Sender<WriterMessage>,
}

impl WriterHandle {
    fn push(&mut self, data: Bytes) -> Result<EventHeader, WriterError> {
        unimplemented!()
    }
    fn push_with_datetime(
        &mut self,
        datetime: DateTime,
        data: Bytes,
    ) -> Result<EventHeader, WriterError> {
        unimplemented!()
    }
}

pub enum WriterMessage {
    Push {
        data: Bytes,
        reply_to: oneshot::Sender<Result<EventHeader, PushError>>,
    },
    PushWithDate {
        data: Bytes,
        datetime: DateTime,
        reply_to: oneshot::Sender<Result<EventHeader, PushError>>,
    },
}

#[derive(Debug, thiserror::Error)]
pub enum WriterError {
    #[error("The channel does not exist")]
    ChannelNotExist,

    #[error("Can not acquire a channel lock")]
    LockError,

    #[error(transparent)]
    StoreError(#[from] StoreError),
}

#[derive(Debug, thiserror::Error)]
pub enum PushError {
    #[error("The datetime must be greater then the last value")]
    BadDateTime,

    #[error(transparent)]
    WriterError(#[from] WriterError),
}
