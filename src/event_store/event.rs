use bytes::Bytes;
use uuid::Uuid;

use crate::time::DateTime;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct EventHeader {
    pub channel: Uuid,
    pub serial: u64,
    pub datetime: DateTime,
    pub checksum: u32,
}

#[derive(Debug, Clone)]
pub struct Event {
    header: EventHeader,
    data: Bytes,
}

impl Event {
    pub fn new(channel: Uuid, serial: u64, datetime: DateTime, data: Bytes) -> Self {
        let checksum = crc32fast::hash(&data);
        let header = EventHeader {
            channel,
            serial,
            datetime,
            checksum,
        };
        Self { header, data }
    }

    pub fn header(&self) -> &EventHeader {
        &self.header
    }

    pub fn data(&self) -> &Bytes {
        &self.data
    }
}
