use uuid::Uuid;

use crate::{
    event_store::{ChannelInfo, EventHeader},
    time::DateTime,
};

use super::RocksDataError;

mod encoding {
    include!(concat!(env!("OUT_DIR"), "/capnp/rocksdb_encoding_capnp.rs"));
}

pub trait Serializable: Sized {
    type Bytes: 'static + AsRef<[u8]>;
    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError>;
    fn serialize(&self) -> Result<Self::Bytes, RocksDataError>;
}

// i64

impl Serializable for i64 {
    type Bytes = [u8; 8];

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        bytes
            .try_into()
            .map(i64::from_be_bytes)
            .map_err(|_| RocksDataError::ParseError("Can not deserialize i64 value"))
    }

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        Ok(self.to_be_bytes())
    }
}

// u32

impl Serializable for u32 {
    type Bytes = [u8; 4];

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        bytes
            .try_into()
            .map(u32::from_be_bytes)
            .map_err(|_| RocksDataError::ParseError("Can not deserialize u32 value"))
    }

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        Ok(self.to_be_bytes())
    }
}

// u64

impl Serializable for u64 {
    type Bytes = [u8; 8];

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        bytes
            .try_into()
            .map(u64::from_be_bytes)
            .map_err(|_| RocksDataError::ParseError("Can not deserialize i64 value"))
    }

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        Ok(self.to_be_bytes())
    }
}

// DateTime

impl Serializable for DateTime {
    type Bytes = <i64 as Serializable>::Bytes;

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        self.timestamp_micros().serialize()
    }

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        Ok(Self::from_timestamp_micros(i64::deserialize(bytes)?))
    }
}

// uuid

impl Serializable for Uuid {
    type Bytes = ::uuid::Bytes;

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        Uuid::from_slice(bytes).map_err(|_| RocksDataError::ParseError("Can not decode UUID"))
    }
    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        Ok(self.into_bytes())
    }
}

// ChannelIndex

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct ChannelIndex(pub(super) u32);



impl From<u32> for ChannelIndex {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

impl From<ChannelIndex> for u32 {
    fn from(value: ChannelIndex) -> Self {
        value.0
    }
}

impl Serializable for ChannelIndex {
    type Bytes = <u32 as Serializable>::Bytes;

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        self.0.serialize()
    }

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        Ok(Self(u32::deserialize(bytes)?))
    }
}

// EventIndex

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct EventIndex {
    pub channel: ChannelIndex,
    pub serial: u64,
}

impl Serializable for EventIndex {
    type Bytes = [u8; 12];

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        // [4, 8]
        let (channel_bytes, remainder) = bytes.split_at(4);
        let serial_bytes = remainder;

        let channel = ChannelIndex::deserialize(channel_bytes)?;
        let serial = u64::deserialize(serial_bytes)?;

        Ok(EventIndex { channel, serial })
    }

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        let mut bytes = [0u8; 12];

        bytes
            .iter_mut()
            .zip(
                self.channel
                    .serialize()?
                    .into_iter()
                    .chain(self.serial.serialize()?),
            )
            .for_each(|(dst, src)| *dst = src);

        Ok(bytes)
    }
}

// EventDateTimeIndex

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct EventDateTimeIndex {
    pub channel: ChannelIndex,
    pub datetime: DateTime,
}

impl Serializable for EventDateTimeIndex {
    type Bytes = [u8; 12];

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        let (channel_bytes, remainder) = bytes.split_at(4);
        let datetime_bytes = remainder;

        let channel = ChannelIndex::deserialize(channel_bytes)?;
        let datetime = DateTime::deserialize(datetime_bytes)?;

        Ok(EventDateTimeIndex { channel, datetime })
    }

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        let mut bytes = [0u8; 12];

        bytes
            .iter_mut()
            .zip(
                self.channel
                    .serialize()?
                    .into_iter()
                    .chain(self.datetime.serialize()?),
            )
            .for_each(|(dst, src)| *dst = src);

        Ok(bytes)
    }
}

// ChannelInfo

impl Serializable for ChannelInfo {
    type Bytes = Box<[u8]>;

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        const ERROR_MSG: &str = "Can not decode channel info";

        let reader =
            ::capnp::serialize::read_message(bytes, ::capnp::message::ReaderOptions::default())
                .map_err(|source| RocksDataError::CapnpDecodeError {
                    context: ERROR_MSG,
                    source,
                })?;
        let root = reader
            .get_root::<encoding::channel_info::Reader>()
            .map_err(|source| RocksDataError::CapnpDecodeError {
                context: ERROR_MSG,
                source,
            })?;

        let uuid = Uuid::deserialize(root.get_uuid().map_err(|source| {
            RocksDataError::CapnpDecodeError {
                context: "Can not decode channel's `uuid` field",
                source,
            }
        })?)?;
        let name = root
            .get_name()
            .map_err(|source| RocksDataError::CapnpDecodeError {
                context: "Can not decode channel's `name` field",
                source,
            })?
            .to_str()?;

        Ok(Self {
            uuid,
            name: name.into(),
        })
    }

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        let mut builder =
            ::capnp::message::TypedBuilder::<encoding::channel_info::Owned>::new_default();
        let mut root = builder.init_root();

        root.set_uuid(self.uuid.serialize()?.as_ref());
        root.set_name(self.name.as_ref().into());

        let mut bytes: Vec<u8> = vec![];
        ::capnp::serialize::write_message(&mut bytes, builder.borrow_inner()).map_err(
            |source| RocksDataError::CapnpEncodeError {
                context: "Can not encode channel value",
                source,
            },
        )?;
        Ok(bytes.into())
    }
}

// EventHeader

impl Serializable for EventHeader {
    type Bytes = Box<[u8]>;

    fn deserialize(bytes: &[u8]) -> Result<Self, RocksDataError> {
        const ERROR_MSG: &str = "Can not decode event header";

        let reader =
            ::capnp::serialize::read_message(bytes, ::capnp::message::ReaderOptions::default())
                .map_err(|source| RocksDataError::CapnpDecodeError {
                    context: ERROR_MSG,
                    source,
                })?;
        let root = reader
            .get_root::<encoding::event_header::Reader>()
            .map_err(|source| RocksDataError::CapnpDecodeError {
                context: ERROR_MSG,
                source,
            })?;

        let channel = Uuid::deserialize(root.get_channel().map_err(|source| {
            RocksDataError::CapnpDecodeError {
                context: "Can not decode event header's channel field",
                source,
            }
        })?)?;
        let serial = root.get_serial();
        let datetime = root.get_datetime().into();
        let checksum = root.get_checksum();

        Ok(EventHeader {
            channel,
            serial,
            datetime,
            checksum,
        })
    }

    fn serialize(&self) -> Result<Self::Bytes, RocksDataError> {
        let mut builder =
            ::capnp::message::TypedBuilder::<encoding::event_header::Owned>::new_default();
        let mut root = builder.init_root();

        root.set_channel(self.channel.serialize()?.as_ref());
        root.set_serial(self.serial);
        root.set_datetime(self.datetime.into());
        root.set_checksum(self.checksum);

        let mut bytes: Vec<u8> = vec![];
        ::capnp::serialize::write_message(&mut bytes, builder.borrow_inner()).map_err(
            |source| RocksDataError::CapnpEncodeError {
                context: "Can not encode event header",
                source,
            },
        )?;
        Ok(bytes.into())
    }
}
