use std::{
    path::{Path, PathBuf},
    sync::{Arc, RwLock},
};

use async_channel;
use bytes::Bytes;
use futures::{Future, TryFutureExt};
use tokio::sync::oneshot;
use uuid::Uuid;

use crate::{
    event_store::{ChannelInfo, CreateError, Direction, Event, EventHeader, StoreError},
    time::DateTime,
    QUEUE_SIZE,
};

use super::{
    codec::{ChannelIndex, EventIndex},
    store::RocksStore,
};

const NUM_READERS: usize = 4;
const READ_QUEUE_SIZE: usize = QUEUE_SIZE * NUM_READERS;

pub fn start_rocks_store(
    path: PathBuf,
) -> (
    (WriteHandle, ReadHandle),
    tokio::task::JoinHandle<Result<(), StoreError>>,
) {
    // TODO: Make better error handling.

    let (reader_tx, reader_rx) = async_channel::bounded(QUEUE_SIZE);
    let (writer_tx, writer_rx) = async_channel::bounded(QUEUE_SIZE);

    let join_handle = tokio::task::spawn(async move {
        let (reader_finished_tx, reader_finished_rx) = oneshot::channel();
        let (writer_finished_tx, writer_finished_rx) = oneshot::channel();

        let store = tokio::task::spawn_blocking(move || -> Result<_, StoreError> {
            Ok(Arc::new(RwLock::new(RocksStore::open(path)?)))
        })
        .await
        .unwrap()?;

        let executor = ReadExecutor {
            inbox: reader_rx.clone(),
            thread_pool: rayon::ThreadPoolBuilder::new()
                .num_threads(NUM_READERS)
                .thread_name(|n| format!("RocksStore reader #{}", n))
                .build()
                .unwrap(),
            store: store.clone(),
        };
        std::thread::Builder::new()
            .name("RocksStore reader".into())
            .spawn(move || {
                reader_finished_tx.send(executor.run());
            });

        let executor = WriteExecutor {
            inbox: writer_rx,
            store,
        };
        std::thread::Builder::new()
            .name("RocksStore writer".into())
            .spawn(move || {
                writer_finished_tx.send(executor.run());
            });

        let (read_result, write_result) = tokio::join!(
            reader_finished_rx
                .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD))),
            writer_finished_rx
                .unwrap_or_else(|_| Err(StoreError::ConnectionError(WRITE_WORKER_IS_DEAD)))
        );

        read_result.and_then(|_| write_result)
    });

    let read_handle = ReadHandle { tx: reader_tx };
    let write_handle = WriteHandle { tx: writer_tx };

    ((write_handle, read_handle), join_handle)
}

// Read

const READ_WORKER_IS_DEAD: &str = "Rocks store read worker is dead";

#[derive(Clone)]
pub struct ReadHandle {
    tx: async_channel::Sender<ReadMessage>,
}

impl ReadHandle {
    pub fn get_channel_info(
        &self,
        channel: ChannelIndex,
    ) -> impl 'static + Future<Output = Result<Option<ChannelInfo>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::GetChannelInfo {
                    channel,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }

    pub fn get_channel_index_by_name(
        &self,
        name: Arc<str>,
    ) -> impl 'static + Future<Output = Result<Option<ChannelIndex>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::GetChannelIndexByName {
                    name,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }

    pub fn get_channel_index_by_uuid(
        &self,
        uuid: Uuid,
    ) -> impl 'static + Future<Output = Result<Option<ChannelIndex>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::GetChannelIndexByUuid {
                    uuid,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }

    pub fn read_channels(
        &self,
        from_index: ChannelIndex,
        limit: usize,
        buffer: Vec<(ChannelIndex, ChannelInfo)>,
    ) -> impl 'static + Future<Output = Result<Vec<(ChannelIndex, ChannelInfo)>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::ReadChannels {
                    from_index,
                    limit,
                    buffer,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }

    pub fn get_first_event_index(
        &self,
        channel: ChannelIndex,
    ) -> impl 'static + Future<Output = Result<Option<EventIndex>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::GetFirstEventIndex {
                    channel,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }

    pub fn get_last_event_index(
        &self,
        channel: ChannelIndex,
    ) -> impl 'static + Future<Output = Result<Option<EventIndex>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::GetLastEventIndex {
                    channel,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }

    pub fn get_event_index_at(
        &self,
        channel: ChannelIndex,
        datetime: DateTime,
    ) -> impl 'static + Future<Output = Result<Option<EventIndex>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::GetEventIndexAt {
                    channel,
                    datetime,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }

    pub fn read_event_headers(
        &self,
        from_index: EventIndex,
        direction: Direction,
        limit: usize,
        buffer: Vec<(EventIndex, EventHeader)>,
    ) -> impl 'static + Future<Output = Result<Vec<(EventIndex, EventHeader)>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::ReadEventHeaders {
                    from_index,
                    direction,
                    limit,
                    buffer,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }

    pub fn read_event_data(
        &self,
        from_index: EventIndex,
        direction: Direction,
        limit: usize,
        buffer: Vec<(EventIndex, Bytes)>,
    ) -> impl 'static + Future<Output = Result<Vec<(EventIndex, Bytes)>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(ReadMessage::ReadEventData {
                    from_index,
                    direction,
                    limit,
                    buffer,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(READ_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(READ_WORKER_IS_DEAD)))
                })
                .await
        }
    }
}

enum ReadMessage {
    GetChannelInfo {
        channel: ChannelIndex,
        reply_to: oneshot::Sender<Result<Option<ChannelInfo>, StoreError>>,
    },
    GetChannelIndexByName {
        name: Arc<str>,
        reply_to: oneshot::Sender<Result<Option<ChannelIndex>, StoreError>>,
    },
    GetChannelIndexByUuid {
        uuid: Uuid,
        reply_to: oneshot::Sender<Result<Option<ChannelIndex>, StoreError>>,
    },
    ReadChannels {
        from_index: ChannelIndex,
        limit: usize,
        buffer: Vec<(ChannelIndex, ChannelInfo)>,
        reply_to: oneshot::Sender<Result<Vec<(ChannelIndex, ChannelInfo)>, StoreError>>,
    },
    GetFirstEventIndex {
        channel: ChannelIndex,
        reply_to: oneshot::Sender<Result<Option<EventIndex>, StoreError>>,
    },
    GetLastEventIndex {
        channel: ChannelIndex,
        reply_to: oneshot::Sender<Result<Option<EventIndex>, StoreError>>,
    },
    GetEventIndexAt {
        channel: ChannelIndex,
        datetime: DateTime,
        reply_to: oneshot::Sender<Result<Option<EventIndex>, StoreError>>,
    },
    ReadEventHeaders {
        from_index: EventIndex,
        direction: Direction,
        limit: usize,
        buffer: Vec<(EventIndex, EventHeader)>,
        reply_to: oneshot::Sender<Result<Vec<(EventIndex, EventHeader)>, StoreError>>,
    },
    ReadEventData {
        from_index: EventIndex,
        direction: Direction,
        limit: usize,
        buffer: Vec<(EventIndex, Bytes)>,
        reply_to: oneshot::Sender<Result<Vec<(EventIndex, Bytes)>, StoreError>>,
    },
}

struct ReadExecutor {
    inbox: async_channel::Receiver<ReadMessage>,
    thread_pool: rayon::ThreadPool,
    store: Arc<RwLock<RocksStore>>,
}

impl ReadExecutor {
    fn run(&self) -> Result<(), StoreError> {
        use rayon::prelude::*;
        let mut queue = Vec::with_capacity(READ_QUEUE_SIZE);
        while let Ok(msg) = self.inbox.recv_blocking() {
            queue.push(msg);
            while queue.len() <= READ_QUEUE_SIZE {
                if let Ok(msg) = self.inbox.try_recv() {
                    queue.push(msg);
                    continue;
                }
                break;
            }
            let store = self.store.read().unwrap();
            queue
                .par_drain(..)
                .for_each(|msg| handle_reade_message(&store, msg));
        }
        Ok(())
    }
}

fn handle_reade_message(store: &RocksStore, msg: ReadMessage) {
    match msg {
        ReadMessage::GetChannelInfo { channel, reply_to } => {
            reply_to.send(store.get_channel_info(channel));
        }
        ReadMessage::GetChannelIndexByName { name, reply_to } => {
            reply_to.send(store.get_channel_index_by_name(name));
        }
        ReadMessage::GetChannelIndexByUuid { uuid, reply_to } => {
            reply_to.send(store.get_channel_index_by_uuid(uuid));
        }
        ReadMessage::ReadChannels {
            from_index,
            limit,
            mut buffer,
            reply_to,
        } => {
            let result = store.read_channels(from_index, limit, &mut buffer);
            reply_to.send(result.map(|()| buffer));
        }
        ReadMessage::GetFirstEventIndex { channel, reply_to } => {
            reply_to.send(store.get_first_event_index(channel));
        }
        ReadMessage::GetLastEventIndex { channel, reply_to } => {
            reply_to.send(store.get_last_event_index(channel));
        }
        ReadMessage::GetEventIndexAt {
            channel,
            datetime,
            reply_to,
        } => {
            reply_to.send(store.get_event_index_at(channel, datetime));
        }
        ReadMessage::ReadEventHeaders {
            from_index,
            direction,
            limit,
            mut buffer,
            reply_to,
        } => {
            let result = store.read_event_headers(from_index, direction, limit, &mut buffer);
            reply_to.send(result.map(|()| buffer));
        }
        ReadMessage::ReadEventData {
            from_index,
            direction,
            limit,
            mut buffer,
            reply_to,
        } => {
            let result = store.read_event_data(from_index, direction, limit, &mut buffer);
            reply_to.send(result.map(|()| buffer));
        }
    }
}

// Write

const WRITE_WORKER_IS_DEAD: &str = "Rocks store write worker is dead";

#[derive(Clone)]
pub struct WriteHandle {
    tx: async_channel::Sender<WriteMessage>,
}

impl WriteHandle {
    pub fn create_channel(
        &self,
        name: Arc<str>,
    ) -> impl 'static + Future<Output = Result<(ChannelIndex, ChannelInfo), CreateError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(WriteMessage::CreateChannel {
                    name,
                    reply_to: reply_tx,
                })
                .map_err(|_| CreateError::from(StoreError::ConnectionError(WRITE_WORKER_IS_DEAD)))
                .and_then(|_| {
                    reply_rx.unwrap_or_else(|_| {
                        Err(CreateError::from(StoreError::ConnectionError(
                            WRITE_WORKER_IS_DEAD,
                        )))
                    })
                })
                .await
        }
    }

    pub fn write_events(
        &self,
        channel: ChannelIndex,
        events: Vec<Event>,
    ) -> impl 'static + Future<Output = Result<Vec<Event>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.tx.clone();
        async move {
            request_tx
                .send(WriteMessage::WriteEvents {
                    channel,
                    buffer: events,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(WRITE_WORKER_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(WRITE_WORKER_IS_DEAD)))
                })
                .await
        }
    }
}

enum WriteMessage {
    CreateChannel {
        name: Arc<str>,
        reply_to: oneshot::Sender<Result<(ChannelIndex, ChannelInfo), CreateError>>,
    },
    WriteEvents {
        channel: ChannelIndex,
        buffer: Vec<Event>,
        reply_to: oneshot::Sender<Result<Vec<Event>, StoreError>>,
    },
}

struct WriteExecutor {
    inbox: async_channel::Receiver<WriteMessage>,
    store: Arc<RwLock<RocksStore>>,
}

impl WriteExecutor {
    fn run(&self) -> Result<(), StoreError> {
        while let Ok(msg) = self.inbox.recv_blocking() {
            let mut store = self.store.write().unwrap();
            handle_write_message(&mut store, msg);
            while let Ok(msg) = self.inbox.try_recv() {
                handle_write_message(&mut store, msg);
            }
        }
        Ok(())
    }
}

fn handle_write_message(store: &mut RocksStore, msg: WriteMessage) {
    match msg {
        WriteMessage::CreateChannel { name, reply_to } => {
            reply_to.send(store.create_channel(name));
        }
        WriteMessage::WriteEvents {
            channel,
            buffer,
            reply_to,
        } => {
            let result = store.write_events(channel, &buffer);
            reply_to.send(result.map(|()| buffer));
        }
    }
}
