use std::{path::PathBuf, sync::Arc};

use bytes::Bytes;
use rocksdb::IteratorMode;
use uuid::Uuid;

use crate::{
    event_store::{ChannelInfo, CreateError, Direction, Event, EventHeader, StoreError},
    time::{DateTime, Duration},
};

use super::{
    codec::{ChannelIndex, EventDateTimeIndex, EventIndex, Serializable},
    RocksDataError,
};

type DB = rocksdb::DB;

const CHANNELS_CF: &str = "channels";
const IX_CHANNELS_UUID_CF: &str = "ix_channels_uuid";
const IX_CHANNELS_NAME_CF: &str = "ix_channels_name";
const EVENT_HEADERS_CF: &str = "event_headers";
const EVENT_DATA_CF: &str = "event_data";
const IX_EVENTS_DATETIME_CF: &str = "ix_events_datetime";

pub struct RocksStore {
    path: PathBuf,
    db: DB,
}

impl RocksStore {
    pub fn open(path: PathBuf) -> Result<RocksStore, StoreError> {
        let mut defautl_opts = rocksdb::Options::default();
        defautl_opts.create_if_missing(true);
        defautl_opts.create_missing_column_families(true);

        let mut events_opts = defautl_opts.clone();
        events_opts.set_prefix_extractor(rocksdb::SliceTransform::create_fixed_prefix(
            std::mem::size_of::<u32>(),
        ));

        let cfs = [
            rocksdb::ColumnFamilyDescriptor::new(CHANNELS_CF, defautl_opts.clone()),
            rocksdb::ColumnFamilyDescriptor::new(IX_CHANNELS_UUID_CF, defautl_opts.clone()),
            rocksdb::ColumnFamilyDescriptor::new(IX_CHANNELS_NAME_CF, defautl_opts.clone()),
            rocksdb::ColumnFamilyDescriptor::new(EVENT_HEADERS_CF, events_opts.clone()),
            rocksdb::ColumnFamilyDescriptor::new(EVENT_DATA_CF, events_opts.clone()),
            rocksdb::ColumnFamilyDescriptor::new(IX_EVENTS_DATETIME_CF, events_opts),
        ];

        let db = DB::open_cf_descriptors(&defautl_opts, &path, cfs)?;

        Ok(RocksStore { path, db })
    }

    fn channels_cf(&self) -> &rocksdb::ColumnFamily {
        self.db
            .cf_handle(CHANNELS_CF)
            .unwrap_or_else(|| panic!("Can not get column family `{}`", CHANNELS_CF))
    }

    fn ix_channels_name_cf(&self) -> &rocksdb::ColumnFamily {
        self.db
            .cf_handle(IX_CHANNELS_NAME_CF)
            .unwrap_or_else(|| panic!("Can not get column family `{}`", IX_CHANNELS_NAME_CF))
    }

    fn ix_channels_uuid_cf(&self) -> &rocksdb::ColumnFamily {
        self.db
            .cf_handle(IX_CHANNELS_UUID_CF)
            .unwrap_or_else(|| panic!("Can not get column family `{}`", IX_CHANNELS_UUID_CF))
    }

    fn event_headers_cf(&self) -> &rocksdb::ColumnFamily {
        self.db
            .cf_handle(EVENT_HEADERS_CF)
            .unwrap_or_else(|| panic!("Can not get column family `{}`", EVENT_HEADERS_CF))
    }

    fn event_data_cf(&self) -> &rocksdb::ColumnFamily {
        self.db
            .cf_handle(EVENT_DATA_CF)
            .unwrap_or_else(|| panic!("Can not get column family `{}`", EVENT_DATA_CF))
    }

    fn ix_events_datetime_cf(&self) -> &rocksdb::ColumnFamily {
        self.db
            .cf_handle(IX_EVENTS_DATETIME_CF)
            .unwrap_or_else(|| panic!("Can not get column family `{}`", IX_EVENTS_DATETIME_CF))
    }

    pub fn create_channel(
        &mut self,
        name: Arc<str>,
    ) -> Result<(ChannelIndex, ChannelInfo), CreateError> {
        // TODO: use transaction in multithreaded db

        let channels_cf = self.channels_cf();
        let ix_channels_name_cf = self.ix_channels_name_cf();
        let ix_channels_uuid_cf = self.ix_channels_uuid_cf();

        let encoded_name = name.as_bytes();

        if self.db.key_may_exist_cf(&ix_channels_name_cf, encoded_name)
            && self
                .db
                .get_pinned_cf(&ix_channels_name_cf, encoded_name)
                .map_err(StoreError::from)?
                .is_some()
        {
            return Err(CreateError::AlreadyExists { name });
        }

        let index =
            if let Some(last_kv) = self.db.iterator_cf(&channels_cf, IteratorMode::End).nth(0) {
                let (key, _value) = last_kv.map_err(StoreError::from)?;
                let index = ChannelIndex::deserialize(key.as_ref()).map_err(|_| {
                    StoreError::from(RocksDataError::ParseError("Can not decode channels key"))
                })?;
                index.next()
            } else {
                0.into()
            };
        let encoded_index = index.serialize().unwrap();

        let (uuid, encoded_uuid) = loop {
            let uuid = crate::utils::new_uuid();
            let encoded_uuid = uuid.serialize().unwrap();
            if self.db.key_may_exist_cf(&ix_channels_uuid_cf, encoded_uuid)
                && self
                    .db
                    .get_pinned_cf(&ix_channels_uuid_cf, encoded_uuid)
                    .map_err(StoreError::from)?
                    .is_some()
            {
                continue;
            }
            break (uuid, encoded_uuid);
        };

        let channel_info = ChannelInfo { uuid, name };
        let encoded_channel_info = channel_info.serialize().map_err(StoreError::from)?;
        let encoded_name = channel_info.name.as_bytes();

        self.db
            .put_cf(&channels_cf, encoded_index, &encoded_channel_info)
            .map_err(StoreError::from)?;
        self.db
            .put_cf(&ix_channels_name_cf, encoded_name, encoded_index)
            .map_err(StoreError::from)?;
        self.db
            .put_cf(&ix_channels_uuid_cf, encoded_uuid, encoded_index)
            .map_err(StoreError::from)?;

        Ok((index, channel_info))
    }

    pub fn get_channel_info(
        &self,
        channel: ChannelIndex,
    ) -> Result<Option<ChannelInfo>, StoreError> {
        if let Some(bytes) = self
            .db
            .get_pinned_cf(self.channels_cf(), channel.serialize()?)?
        {
            Ok(Some(ChannelInfo::deserialize(bytes.as_ref())?))
        } else {
            Ok(None)
        }
    }

    pub fn get_channel_index_by_name(
        &self,
        name: impl AsRef<str>,
    ) -> Result<Option<ChannelIndex>, StoreError> {
        let encoded_name = name.as_ref().as_bytes();
        if let Some(bytes) = self
            .db
            .get_pinned_cf(self.ix_channels_name_cf(), encoded_name)?
        {
            Ok(Some(ChannelIndex::deserialize(bytes.as_ref())?))
        } else {
            Ok(None)
        }
    }

    pub fn get_channel_index_by_uuid(
        &self,
        uuid: Uuid,
    ) -> Result<Option<ChannelIndex>, StoreError> {
        if let Some(bytes) = self
            .db
            .get_pinned_cf(self.ix_channels_uuid_cf(), uuid.serialize()?.as_slice())?
        {
            Ok(Some(ChannelIndex::deserialize(bytes.as_ref())?))
        } else {
            Ok(None)
        }
    }

    pub fn read_channels(
        &self,
        from_index: ChannelIndex,
        limit: usize,
        buffer: &mut Vec<(ChannelIndex, ChannelInfo)>,
    ) -> Result<(), StoreError> {
        fn decode(
            index: ChannelIndex,
            value: &[u8],
        ) -> Result<(ChannelIndex, ChannelInfo), RocksDataError> {
            Ok((index, ChannelInfo::deserialize(value)?))
        }
        read_rows(
            &self.db,
            self.channels_cf(),
            from_index,
            Direction::Forward,
            limit,
            decode,
            buffer,
        )
    }

    pub fn get_first_event_index(
        &self,
        channel: ChannelIndex,
    ) -> Result<Option<EventIndex>, StoreError> {
        let index = EventIndex { channel, serial: 0 };

        let mut iter = self.db.raw_iterator_cf(self.event_headers_cf());
        iter.seek(index.serialize()?);
        iter.status()?;

        if let Some(bytes) = iter.key() {
            Ok(Some(EventIndex::deserialize(bytes)?))
        } else {
            Ok(None)
        }
    }

    pub fn get_last_event_index(
        &self,
        channel: ChannelIndex,
    ) -> Result<Option<EventIndex>, StoreError> {
        let index = EventIndex {
            channel,
            serial: u64::MAX,
        };

        let mut iter = self.db.raw_iterator_cf(self.event_headers_cf());
        iter.seek_for_prev(index.serialize()?);
        iter.status()?;

        if let Some(bytes) = iter.key() {
            Ok(Some(EventIndex::deserialize(bytes)?))
        } else {
            Ok(None)
        }
    }

    pub fn get_event_index_at(
        &self,
        channel: ChannelIndex,
        datetime: DateTime,
    ) -> Result<Option<EventIndex>, StoreError> {
        let index = EventDateTimeIndex { channel, datetime };

        let mut iter = self.db.raw_iterator_cf(self.ix_events_datetime_cf());
        iter.seek_for_prev(index.serialize()?);
        iter.status()?;
        if let Some(bytes) = iter.value() {
            Ok(Some(EventIndex {
                channel,
                serial: u64::deserialize(bytes)?,
            }))
        } else {
            Ok(None)
        }
    }

    pub fn read_event_headers(
        &self,
        from_index: EventIndex,
        direction: Direction,
        limit: usize,
        buffer: &mut Vec<(EventIndex, EventHeader)>,
    ) -> Result<(), StoreError> {
        fn decode(
            index: EventIndex,
            value: &[u8],
        ) -> Result<(EventIndex, EventHeader), RocksDataError> {
            Ok((index, EventHeader::deserialize(value)?))
        }
        match direction {
            Direction::Forward => read_rows(
                &self.db,
                self.event_headers_cf(),
                from_index,
                Direction::Forward,
                limit,
                decode,
                buffer,
            ),
            Direction::Reversed => read_rows(
                &self.db,
                self.event_headers_cf(),
                from_index,
                Direction::Reversed,
                limit,
                decode,
                buffer,
            ),
        }
    }

    pub fn read_event_data(
        &self,
        from_index: EventIndex,
        direction: Direction,
        limit: usize,
        buffer: &mut Vec<(EventIndex, Bytes)>,
    ) -> Result<(), StoreError> {
        fn decode(index: EventIndex, value: &[u8]) -> Result<(EventIndex, Bytes), RocksDataError> {
            Ok((index, Bytes::copy_from_slice(value)))
        }
        match direction {
            Direction::Forward => read_rows(
                &self.db,
                self.event_data_cf(),
                from_index,
                Direction::Forward,
                limit,
                decode,
                buffer,
            ),
            Direction::Reversed => read_rows(
                &self.db,
                self.event_data_cf(),
                from_index,
                Direction::Reversed,
                limit,
                decode,
                buffer,
            ),
        }
    }

    pub fn write_events<'a>(
        &self,
        channel: ChannelIndex,
        events: impl IntoIterator<Item = &'a Event>,
    ) -> Result<(), StoreError> {
        let event_headers_cf = self.event_headers_cf();
        let event_data_cf = self.event_data_cf();
        let ix_events_datetime_cf = self.ix_events_datetime_cf();

        for event in events.into_iter() {
            let index = EventIndex {
                channel,
                serial: event.header().serial,
            };
            let index_encoded = index.serialize()?;
            let header_encoded = event.header().serialize()?;
            let serial_encoded = event.header().serial.serialize()?;
            let datetime_index_encoded = EventDateTimeIndex {
                channel,
                datetime: event.header().datetime,
            }
            .serialize()?;

            // TODO: Wrap this into transaction
            self.db
                .put_cf(event_data_cf, index_encoded.as_ref(), event.data().as_ref())?;
            self.db.put_cf(
                event_headers_cf,
                index_encoded.as_ref(),
                header_encoded.as_ref(),
            )?;
            self.db.put_cf(
                ix_events_datetime_cf,
                datetime_index_encoded.as_ref(),
                serial_encoded.as_ref(),
            )?;
        }

        Ok(())
    }
}

#[inline(always)]
fn read_rows<I, T, F>(
    db: &DB,
    cf: &rocksdb::ColumnFamily,
    from_index: I,
    direction: Direction,
    limit: usize,
    decode: F,
    buffer: &mut Vec<T>,
) -> Result<(), StoreError>
where
    I: Index,
    T: 'static + Sized,
    F: Fn(I, &[u8]) -> Result<T, RocksDataError>,
{
    let mut iter = db.raw_iterator_cf(cf);

    match direction {
        Direction::Forward => iter.seek(from_index.serialize()?),
        Direction::Reversed => iter.seek_for_prev(from_index.serialize()?),
    }

    for _ in 0..limit {
        iter.status()?;
        if let Some((key, value)) = iter.item() {
            let row_index = I::deserialize(key)?;
            if !row_index.is_same_series(&from_index) {
                break;
            }
            buffer.push(decode(row_index, value)?);
            match direction {
                Direction::Forward => iter.next(),
                Direction::Reversed => iter.prev(),
            }
        } else {
            break;
        }
    }

    Ok(())
}

// Indices

pub trait Index: Serializable {
    const MIN: Self;
    const MAX: Self;
    fn is_same_series(&self, other: &Self) -> bool;
    fn next(&self) -> Self;
}

impl Index for ChannelIndex {
    const MIN: Self = ChannelIndex(u32::MIN);
    const MAX: Self = ChannelIndex(u32::MAX);

    fn is_same_series(&self, _other: &Self) -> bool {
        true
    }

    fn next(&self) -> Self {
        (u32::from(*self) + 1).into()
    }
}

impl Index for EventIndex {
    const MIN: Self = Self {
        channel: ChannelIndex::MIN,
        serial: u64::MIN,
    };
    const MAX: Self = Self {
        channel: ChannelIndex::MAX,
        serial: u64::MAX,
    };

    fn is_same_series(&self, other: &Self) -> bool {
        self.channel == other.channel
    }

    fn next(&self) -> Self {
        Self {
            serial: self.serial + 1,
            ..*self
        }
    }
}

impl Index for EventDateTimeIndex {
    const MIN: Self = Self {
        channel: ChannelIndex::MIN,
        datetime: DateTime::MIN,
    };
    const MAX: Self = Self {
        channel: ChannelIndex::MAX,
        datetime: DateTime::MAX,
    };

    fn is_same_series(&self, other: &Self) -> bool {
        self.channel == other.channel
    }

    fn next(&self) -> Self {
        Self {
            datetime: self.datetime + Duration::from(1),
            ..*self
        }
    }
}

// Tests

#[cfg(test)]
mod tests {
    use std::collections::BTreeSet;

    use uuid::Uuid;

    use crate::{
        event_store::{rocksdb::imp::codec::Serializable, ChannelInfo, Direction, Event},
        time::{DateTime, Duration},
    };

    use super::{ChannelIndex, EventDateTimeIndex, EventIndex, Index, RocksStore};

    #[derive(serde::Serialize, serde::Deserialize)]
    struct TestEvent {
        channel: String,
        serial: usize,
    }

    #[test]
    fn event_index() {
        let value = EventIndex {
            channel: 42.into(),
            serial: 69,
        };

        let bytes = value.serialize().unwrap();
        let result = EventIndex::deserialize(bytes.as_slice()).unwrap();

        assert_eq!(value, result);
    }

    #[test]
    fn event_datetime_index() {
        let value = EventDateTimeIndex {
            channel: 42.into(),
            datetime: DateTime::now(),
        };

        let bytes = value.serialize().unwrap();
        let result = EventDateTimeIndex::deserialize(bytes.as_slice()).unwrap();

        assert_eq!(value, result);
    }

    #[test]
    fn create_storage() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();

        let store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        drop(store);
    }

    fn create_three_channels(store: &mut RocksStore) -> Vec<(ChannelIndex, ChannelInfo)> {
        ["foo", "bar", "baz"]
            .into_iter()
            .map(|name| store.create_channel(name.to_string().into()).unwrap())
            .collect()
    }

    #[test]
    fn create_channels() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let channels = create_three_channels(&mut store);
        assert_eq!(
            channels
                .iter()
                .map(|(index, _)| index)
                .cloned()
                .collect::<BTreeSet<_>>()
                .len(),
            3
        );
        assert_eq!(
            channels
                .iter()
                .map(|(_, ChannelInfo { uuid, .. })| uuid)
                .cloned()
                .collect::<BTreeSet<_>>()
                .len(),
            3
        );
        assert!(channels
            .iter()
            .map(|(_, ChannelInfo { name, .. })| name)
            .zip(["foo", "bar", "baz"].into_iter())
            .all(|(a, b)| **a == *b));
    }

    #[test]
    fn list_channels() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let channels = create_three_channels(&mut store);

        let mut listed_channels = vec![];
        store
            .read_channels(channels.first().unwrap().0, 255, &mut listed_channels)
            .unwrap();
        assert_eq!(listed_channels.as_slice(), channels.as_slice());

        listed_channels.clear();
        store
            .read_channels(channels.last().unwrap().0, 255, &mut listed_channels)
            .unwrap();
        assert_eq!(listed_channels.len(), 1);
        assert_eq!(listed_channels.first(), channels.last());
    }

    #[test]
    fn get_channel() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let channels = create_three_channels(&mut store);

        channels.iter().for_each(|(index, info)| {
            let result = store.get_channel_info(*index).unwrap();
            assert_eq!(result.as_ref(), Some(info));
        });
    }

    #[test]
    fn get_non_existent_channel() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let channels = create_three_channels(&mut store);

        let index = channels.last().unwrap().0.next();
        let result = store.get_channel_info(index).unwrap();
        assert_eq!(result, None);
    }

    #[test]
    fn get_channel_index_by_name() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let channels = create_three_channels(&mut store);

        channels.iter().for_each(|(index, info)| {
            let result = store.get_channel_index_by_name(info.name.as_ref()).unwrap();
            assert_eq!(result.as_ref(), Some(index))
        });
    }

    #[test]
    fn get_non_existent_channel_index_by_name() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let _ = create_three_channels(&mut store);

        let result = store.get_channel_index_by_name("abyrvalg").unwrap();
        assert_eq!(result, None)
    }

    #[test]
    fn get_channel_index_by_uuid() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let channels = create_three_channels(&mut store);

        channels.iter().for_each(|(index, info)| {
            let result = store.get_channel_index_by_uuid(info.uuid).unwrap();
            assert_eq!(result.as_ref(), Some(index))
        });
    }

    #[test]
    fn get_non_existent_channel_index_by_uuid() {
        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let _ = create_three_channels(&mut store);

        let result = store.get_channel_index_by_uuid(Uuid::new_v4()).unwrap();
        assert_eq!(result, None)
    }

    fn add_test_events(store: &mut RocksStore, channel: ChannelIndex, num_events: usize) {
        let channel_info = store
            .get_channel_info(channel)
            .unwrap()
            .expect("Channel not exists");
        let datetime = DateTime::now();
        let events: Vec<Event> = (0..num_events)
            .map(|n| {
                let event = TestEvent {
                    channel: channel_info.name.to_string(),
                    serial: n,
                };
                Event::new(
                    channel_info.uuid,
                    n as u64,
                    datetime + Duration::from_micros(n as i64),
                    toml::to_string(&event).unwrap().into_bytes().into(),
                )
            })
            .collect();
        store.write_events(channel, events.iter()).unwrap();
    }

    fn create_three_channels_with_events(
        store: &mut RocksStore,
        num_events: usize,
    ) -> Vec<(ChannelIndex, ChannelInfo)> {
        let channels = create_three_channels(store);
        channels
            .iter()
            .for_each(|(channel, _)| add_test_events(store, *channel, num_events));
        channels
    }

    #[test]
    fn read_events() {
        const NUM_EVENTS: usize = 1024;

        let tmp_dir = ::mktemp::Temp::new_dir().unwrap();
        let mut store = RocksStore::open(tmp_dir.as_path().into()).unwrap();

        let channels = create_three_channels_with_events(&mut store, NUM_EVENTS);

        for (channel, channel_info) in channels.into_iter() {
            let mut event_headers = vec![];
            let mut event_data = vec![];

            let first_index = store.get_first_event_index(channel).unwrap().unwrap();

            store
                .read_event_headers(
                    first_index,
                    Direction::Forward,
                    usize::MAX,
                    &mut event_headers,
                )
                .unwrap();
            store
                .read_event_data(first_index, Direction::Forward, usize::MAX, &mut event_data)
                .unwrap();

            assert_eq!(event_headers.len(), NUM_EVENTS);
            assert_eq!(event_data.len(), NUM_EVENTS);

            for (n, ((header_index, header), (data_index, data))) in event_headers
                .into_iter()
                .zip(event_data.into_iter())
                .enumerate()
            {
                assert_eq!(header_index.channel, channel);
                assert_eq!(data_index.channel, channel);

                assert_eq!(header_index.serial, n as u64);
                assert_eq!(data_index.serial, n as u64);

                assert_eq!(header.channel, channel_info.uuid);
                assert_eq!(header.serial, n as u64);

                // println!("{} {:?} {} {:?}", n, header_index.channel, header_index.serial, &data);
                let event: TestEvent =
                    toml::from_str(&String::from_utf8(data.into()).unwrap()).unwrap();

                assert_eq!(event.channel.as_str(), channel_info.name.as_ref());
                assert_eq!(event.serial, n);
            }
        }
    }
}
