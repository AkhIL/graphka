use crate::event_store::StoreError;

mod codec;
mod async_store;
mod store;

pub use {
    codec::{ChannelIndex, EventIndex},
    async_store::{start_rocks_store, ReadHandle, WriteHandle},
    store::Index,
};

#[derive(Debug, thiserror::Error)]
pub enum RocksDataError {
    #[error("{context}: {source}")]
    CapnpDecodeError {
        context: &'static str,
        source: capnp::Error,
    },

    #[error("{context}: {source}")]
    CapnpEncodeError {
        context: &'static str,
        source: capnp::Error,
    },

    // #[error("{context}: {source}")]
    // TomlParseError {
    //     context: &'static str,
    //     source: toml::de::Error,
    // },
    #[error("{0}")]
    ParseError(&'static str),

    #[error(transparent)]
    UtfError(#[from] std::str::Utf8Error),
}

#[derive(Debug, thiserror::Error)]
pub enum RocksDatabaseError {
    #[error("Can not lock database for writing: {0}")]
    LockError(#[from] lockfile::Error),

    #[error("Directory not exists `{path}`")]
    NotExist { path: std::path::PathBuf },

    #[error("Directory or file already exists `{path}`")]
    AlreadyExist { path: std::path::PathBuf },

    #[error(transparent)]
    IoError(#[from] std::io::Error),

    #[error(transparent)]
    RocksDbError(#[from] rocksdb::Error),
}

impl From<RocksDataError> for StoreError {
    fn from(value: RocksDataError) -> Self {
        Self::DataError(value.into())
    }
}

impl From<RocksDatabaseError> for StoreError {
    fn from(value: RocksDatabaseError) -> Self {
        Self::StoreError(value.into())
    }
}

impl From<rocksdb::Error> for StoreError {
    fn from(value: rocksdb::Error) -> Self {
        RocksDatabaseError::from(value).into()
    }
}
