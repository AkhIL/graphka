use tokio::sync::mpsc;

use super::imp;
use crate::event_store::StoreMessage;

struct Store {
    inbox: mpsc::Receiver<StoreMessage>,
    read_handle: imp::ReadHandle,
    write_handle: imp::WriteHandle,
}
