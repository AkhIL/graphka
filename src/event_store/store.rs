use std::sync::Arc;

use crate::QUEUE_SIZE;
use futures::{Future, FutureExt, Stream, TryFutureExt, TryStreamExt};
use tokio::sync::{mpsc, oneshot};
use tokio_stream::wrappers::ReceiverStream;
use uuid::Uuid;

use super::{ChannelError, ChannelHandle, ChannelInfo, STORE_SERVICE_IS_DEAD};

#[derive(Clone)]
pub struct StoreHandle {
    inbox: mpsc::Sender<StoreMessage>,
}

impl StoreHandle {
    pub fn new(inbox: mpsc::Sender<StoreMessage>) -> Self {
        Self { inbox }
    }

    pub fn list_channels(&self) -> impl 'static + Stream<Item = Result<ChannelInfo, StoreError>> {
        let (reply_tx, reply_rx) = mpsc::channel(QUEUE_SIZE);
        let request_tx = self.inbox.clone();
        async move {
            request_tx
                .send(StoreMessage::ListChannels { reply_to: reply_tx })
                .map_err(|_| StoreError::ConnectionError(STORE_SERVICE_IS_DEAD))
                .map_ok(|_| ReceiverStream::from(reply_rx))
                .await
        }
        .into_stream()
        .try_flatten()
    }

    pub fn get_channel(
        &self,
        uuid: Uuid,
    ) -> impl 'static + Future<Output = Result<Option<ChannelHandle>, StoreError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.inbox.clone();
        async move {
            request_tx
                .send(StoreMessage::GetChannel {
                    uuid,
                    reply_to: reply_tx,
                })
                .map_err(|_| StoreError::ConnectionError(STORE_SERVICE_IS_DEAD))
                .and_then(|_| {
                    reply_rx
                        .unwrap_or_else(|_| Err(StoreError::ConnectionError(STORE_SERVICE_IS_DEAD)))
                })
                .await
        }
    }

    pub fn create_channel(
        &self,
        name: Arc<str>,
    ) -> impl 'static + Future<Output = Result<ChannelHandle, CreateError>> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let request_tx = self.inbox.clone();
        async move {
            request_tx
                .send(StoreMessage::CreateChannel {
                    name,
                    reply_to: reply_tx,
                })
                .map_err(|_| CreateError::from(StoreError::ConnectionError(STORE_SERVICE_IS_DEAD)))
                .and_then(|_| {
                    reply_rx.unwrap_or_else(|_| {
                        Err(CreateError::from(StoreError::ConnectionError(
                            STORE_SERVICE_IS_DEAD,
                        )))
                    })
                })
                .await
        }
    }
}

impl From<mpsc::Sender<StoreMessage>> for StoreHandle {
    fn from(tx: mpsc::Sender<StoreMessage>) -> Self {
        Self { inbox: tx }
    }
}

pub enum StoreMessage {
    ListChannels {
        reply_to: mpsc::Sender<Result<ChannelInfo, StoreError>>,
    },
    GetChannel {
        uuid: Uuid,
        reply_to: oneshot::Sender<Result<Option<ChannelHandle>, StoreError>>,
    },
    CreateChannel {
        name: Arc<str>,
        reply_to: oneshot::Sender<Result<ChannelHandle, CreateError>>,
    },
}

#[derive(Debug, thiserror::Error)]
pub enum StoreError {
    #[error("Store error: {0}")]
    StoreError(anyhow::Error),

    #[error("Data error: {0}")]
    DataError(anyhow::Error),

    #[error("Store connection error")]
    ConnectionError(&'static str),
}

#[derive(Debug, thiserror::Error)]
pub enum CreateError {
    #[error("Channel with name `{name}` already exists")]
    AlreadyExists { name: Arc<str> },

    #[error(transparent)]
    StoreError(#[from] StoreError),
}
