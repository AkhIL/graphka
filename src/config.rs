use std::path::PathBuf;

#[derive(Clone, Debug)]
pub struct Config {
    pub data_dir: PathBuf,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            data_dir: "./graphka_data".into(),
        }
    }
}
