use std::net::SocketAddr;

use futures::Future;

pub struct Server {
    rpc_address: SocketAddr,
}

impl Default for Server {
    fn default() -> Self {
        Self {
            rpc_address: "127.0.0.1:0".parse().unwrap(),
        }
    }
}

impl Server {
    pub fn new() -> Self {
        Default::default()
    }
    pub fn with_rpc_address(self, rpc_address: SocketAddr) -> Self {
        Self { rpc_address }
    }

    pub async fn run(self, shutdown: impl Future<Output = ()>) -> anyhow::Result<()> {
        let (api_handle, api_server) = crate::api::Server::new();
        let rpc_server = crate::capnp_rpc::Server::new(api_handle.clone(), self.rpc_address);

        let api_task = tokio::task::spawn(api_server.run());
        let rpc_task = tokio::task::spawn_local(rpc_server.run());

        shutdown.await;

        rpc_task.abort();
        rpc_task.await.unwrap_err();

        drop(api_handle);
        api_task.await?;

        Ok(())
    }
}
