# graphka

A system for organizing a data processing graph consisting of processor nodes and messages transmitted between them.

The system allows you to view the state of the graph at any point of time, as well as do simulations with previously saved data.

Like Kafka, but Grafka.