@0xb6717f3b0e67c1ca;

using Common = import "common.capnp";

struct ChannelInfo @0xdfed650aee698737 {
	uuid @0 :Data;
	name @1 :Text;
}

struct EventHeader @0x9ba3dca631efb514 {
	channel @0 :Data;  # Channel's UUID.
	serial @1 :UInt64;
	datetime @2 :Common.DateTime;
	checksum @3 :UInt32;
}
